# Mongodb-Marathon
##### MongoDB on Mesos/Marathon with auto-joining replicas

## Intro

Mongodb-Marathon is a Docker container that contains MongoDB and a marathon event subscriber that can listen for other mongos in the same replica set and add/remove them as needed. This container also includes proper replica intialization so there's no manual setup invloved. You can just deploy as many of these as you need and they will self-configure. 

## Getting started

Coming soon....
