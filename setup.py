#!/usr/bin/env python3

PROJECT = 'mongomarathon'
VERSION = '0.1'

from setuptools import setup, find_packages

try:
    long_description = open('README.rst', 'rt').read()
except IOError:
    long_description = 'A Marathon listener to update MongoDB replica sets'

setup(
    name=PROJECT,
    version=VERSION,
    description='A Marathon listener to update MongoDB replica sets',
    long_description=long_description,
    url='https://bitbucket.org/hipchat/mongodb-marathon',
    # Author details
    author='Atlassian, Inc.',
    author_email='hipchat@atlassian.com',
    
    platforms=['Any'],
    install_requires=['gunicorn','kazoo >= 2.2.1','flask >= 0.10.1','requests','pymongo'],
    tests_require=['nose >= 1.3.7','coverage>=4.0a5','mongobox'],
    packages=find_packages(),
    include_package_data=True,
    package_data = {
        
        },
    entry_points={},
)