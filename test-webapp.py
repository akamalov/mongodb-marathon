import os
import json
from mongobox.unittest import MongoTestCase
from mongobox import MongoBox

# we need to set env vars before we import the webapp so it will inherit our changes
os.environ["PORT0"] = os.environ.get("MONGOBOX_PORT",0)
os.environ["REPLICA_SET"] = "mongorepl"
os.environ["MARATHON_APP_ID"] = "/someapp"

import webapp
import unittest

# yeah, this sucks, but the mongo db is not isolated, so we need tests to run in order in the file
unittest.TestLoader.sortTestMethodsUsing = None

initiated = False

class MongoConfigTest(MongoTestCase):

    def setUp(self):
        i = globals().get('initiated')
        if not i:
            webapp.run_initiate(self._check_initiate, self._marathon_subscribe)
            globals()['initiated'] = True
        
    def tearDown(self):
        self.purge_database()

    def test_config(self):
        REPLICA_SET=os.environ.get("REPLICA_SET","repl0")
        mconfig = self.mongo_client.admin.command("replSetGetConfig")
        
        self.assertEqual(mconfig['config']['_id'], REPLICA_SET)

        self.purge_database()

    def test_adding_member(self):
        REPLICA_SET=os.environ.get("REPLICA_SET","repl0")
        event = self._get_running_event()
        
        # boot up the second node on our known port
        box2 = MongoBox(replset=REPLICA_SET, port=event['ports'][0])
        box2.start()
        
        webapp.add_mongo_member(event)
        
        mconfig = self.mongo_client.admin.command("replSetGetConfig")

        self.assertEqual(mconfig['config']['_id'], REPLICA_SET)
        self.assertEqual(2, len(mconfig['config']['members']))
        
        self.purge_by_client(box2.client())
        box2.stop()

        self.purge_database()

    def test_removing_member(self):
        REPLICA_SET=os.environ.get("REPLICA_SET","repl0")
        revent = self._get_running_event()
        fevent = self._get_finished_event()

        # boot up the second node on our known port
        box2 = MongoBox(replset=REPLICA_SET, port=revent['ports'][0])
        box2.start()

        mconfig = self.mongo_client.admin.command("replSetGetConfig")

        self.assertEqual(mconfig['config']['_id'], REPLICA_SET)
        self.assertEqual(2, len(mconfig['config']['members']))

        self.purge_by_client(box2.client())
        box2.stop()

        webapp.remove_mongo_member(fevent)

        mconfig = self.mongo_client.admin.command("replSetGetConfig")
        self.assertEqual(mconfig['config']['_id'], REPLICA_SET)
        self.assertEqual(1, len(mconfig['config']['members']))

        self.purge_database()
        
    def _get_running_event(self):
        with open("testdata/running-event.json") as in_file:
            return json.load(in_file)

    def _get_finished_event(self):
        with open("testdata/finished-event.json") as in_file:
            return json.load(in_file)
        
    def _check_initiate(self):
        return True
        
    def _marathon_subscribe(self):
        return ""

    def purge_by_client(self, client, drop=True):
        client1 = self.mongo_client
        client2 = client

        self.__mongo_client = client2
        self.purge_database(drop=drop)
        self.__mongo_client = client1