FROM ubuntu:14.04
MAINTAINER Jonathan Doklovic <doklovic@atlassian.com>

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

# Installation:
# Import MongoDB public GPG key AND create a MongoDB list file
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
RUN echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.0.list

RUN apt-get update && \
  apt-get install -y software-properties-common && \
  apt-get install -y mongodb-org && \
  apt-get install -y python3-dev python3-setuptools python3-pip curl nano supervisor

RUN update-rc.d -f supervisor disable
ADD conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ADD script/startup /usr/local/bin/startup
RUN chmod +x /usr/local/bin/startup

RUN pip3 install gunicorn setproctitle

# Install gunicorn running script
ADD script/runwebapp /usr/local/bin/runwebapp
RUN chmod +x /usr/local/bin/runwebapp

ADD . /mongomarathon
WORKDIR /mongomarathon
RUN mkdir -p /mongomarathon/logs
RUN mkdir -p /data/db

RUN pip3 install -r /mongomarathon/requirements.txt

EXPOSE 80
EXPOSE 9000
EXPOSE 9090

CMD ["/usr/local/bin/startup"]

