import os.path, time
import logging
import sys
from flask import Flask
from flask import Response, request
from kazoo.client import KazooClient
from pymongo import MongoClient
import requests

app = Flask(__name__)

ZKHOST=os.environ.get("ZK_HOST", "leader.mesos")
ZKPORT=int(os.environ.get("ZK_PORT", 2181))
MARATHON_APP_ID=os.environ.get("MARATHON_APP_ID","/mongodb")
ZKPATH=os.environ.get("ZK_PATH",os.environ.get("MARATHON_APP_ID","/mongodb"))
MYHOST=os.environ.get("HOST","127.0.0.1")
MYPORT=int(os.environ.get("PORT0",6000))
REPLICA_SET=os.environ.get("REPLICA_SET","repl0")
MARATHON_HOST=os.environ.get("MARATHON_HOST", "leader.mesos")
MARATHON_PORT=int(os.environ.get("MARATHON_PORT", 8080))
V2_EVENTS_URI = "http://%s:%s/v2/eventSubscriptions" % (MARATHON_HOST, MARATHON_PORT)


def setup_logging():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)

    app.logger.addHandler(root)
    
setup_logging()

def check_initiate():
    firstrun = False

    # connect to zookeeper
    zk = KazooClient(hosts='%s:%s' %(ZKHOST,ZKPORT))
    zk.start()

    # check our path
    if zk.exists(ZKPATH):
        pass
    else:
        zk.ensure_path(ZKPATH)
        firstrun = True

    # disconnect
    zk.stop()

    return firstrun

def _update_mongo_config(client, config):
    newversion = config['version']
    newversion += 1
    config['version'] = newversion

    client.admin.command('replSetReconfig',config)
    
def add_mongo_member(event):
    mclient = MongoClient(MYHOST, MYPORT, replicaset=REPLICA_SET)
    oldconfig = mclient.admin.command("replSetGetConfig")
    newconfig = oldconfig['config'].copy()
    
    members = newconfig['members']
    newid = max([member['_id'] for member in members]) + 1
    members.append({'_id': newid, 'host': '%s:%s' %(event['host'], event['ports'][0])})
    
    newconfig['members'] = members
    _update_mongo_config(mclient,newconfig)
    
    mclient.close()
    
    
def remove_mongo_member(event):
    mclient = MongoClient(MYHOST, MYPORT, replicaset=REPLICA_SET)
    oldconfig = mclient.admin.command("replSetGetConfig")
    newconfig = oldconfig['config'].copy()
    members = newconfig['members']

    remove_idx = next((index for (index, item) in enumerate(members) if item['host'] == '%s:%s' % (event['host'], event['ports'][0])),-1)
    
    if remove_idx > -1:
        members.pop(remove_idx)
        newconfig['members'] = members
        _update_mongo_config(mclient,newconfig)
        
    mclient.close()

def i_am_master():
    mclient = MongoClient(MYHOST, MYPORT, replicaset=REPLICA_SET)
    isMaster = mclient.is_primary
    mclient.close()
    
    return isMaster
    
def check_reconfigure(event):
    # since the master is the only one who can update replica configs, let's pass if we're a slave
    if not i_am_master:
        return
    
    # we only want events that look like this:
    # {
    #     "eventType": "status_update_event",
    #     "timestamp": "2014-03-01T23:29:30.158Z",
    #     "slaveId": "20140909-054127-177048842-5050-1494-0",
    #     "taskId": "my-app_0-1396592784349",
    #     "taskStatus": "TASK_RUNNING",
    #     "appId": "/my-app",
    #     "host": "slave-1234.acme.org",
    #     "ports": [31372],
    #     "version": "2014-04-04T06:26:23.051Z"
    # }

    status_actions = {"TASK_RUNNING": add_mongo_member,
                         "TASK_FINISHED": remove_mongo_member,
                         "TASK_FAILED": remove_mongo_member,
                         "TASK_KILLED": remove_mongo_member,
                         "TASK_LOST": remove_mongo_member,
                         }
    
    if event['eventType'] == 'status_update_event' and event['appId'] == MARATHON_APP_ID:
        status = event['taskStatus']
        try:
            status_actions[status](event)
        except KeyError:
            pass

def register_marathon_subscriber():
    headers = {'Accept' : 'application/json'}

    cbparam = {'callbackUrl': 'http://%s:%s/events'} % (MYHOST, MYPORT)
    
    #TODO: check response and deal with non 200
    jsonresponse = requests.post(V2_EVENTS_URI, params=cbparam, headers=headers).json()

@app.route('/events', methods=['POST'])
def event_receiver():
    event = request.get_json()
    check_reconfigure(event)

@app.route('/', methods = ['GET'])
def healthcheck():
    resp = Response("hello", status=200)
    return resp

def run_initiate(check_initiate_fn,marathon_register_fn):
    # check to see if we're the first mongo. if so, put a lock in zookeeper and initiate the replica set
    if(check_initiate_fn()):
        # initiate our mongodb replica set
        mclient = MongoClient(MYHOST, MYPORT)
        config = {'_id': REPLICA_SET, 'members': 
                    [
                        {'_id': 0, 'host': '%s:%s' %(MYHOST, MYPORT)}
                    ]
                }
        mclient.admin.command("replSetInitiate", config)
        mclient.close()
        
    marathon_register_fn()

@app.before_first_request
def setup_webapp():
    run_initiate(check_initiate,register_marathon_subscriber)
    
if __name__ == '__main__':
    app.run()